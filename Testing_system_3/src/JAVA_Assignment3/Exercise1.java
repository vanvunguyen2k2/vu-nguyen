package JAVA_Assignment3;

import java.util.Random;
import java.util.Scanner;

public class Exercise1 {
    public static void main(String[] args) {
        Exercise1 exercise1 = new Exercise1();
        exercise1.question1();
        exercise1.question2();
        exercise1.question3();
        exercise1.question4();
    }

    public void question1(){
        System.out.println("------cau1-----");
        // KHAI BAO 2 BIEN FLOAT VA LUONG
        float value1;
        float value2;
        value1 = (float) 5240.5;
        value2 = (float) 10970.055;

        // LAM TRON 2 SO FLOAT VA IN CHUNG RA
        int i = (int) value1;
        System.out.println("value1 1 duoc lam tron la: " + i);

        int i2 = (int) value2;
        System.out.println("value2 duoc lam tron la: " + i2);
    }

    public void question2(){
        System.out.println("------CAU2-----");
        // lay ngau nhien 1 so co 5 chu so
        Random random = new Random();
        int i = 0;
        int i2 = 99999;
        int i3 = random.nextInt((i2-i) + 1);
        while (i3 < 10000){
            i3 = i3 * 10;

        }System.out.println(i3);
    }

    public void question3(){
        System.out.println("-------Cau3-------");
        Random random = new Random();
        int y = 0;
        int y2 = 99999;
        int y3 = random.nextInt((y2-y) + 1);
        String s = String.valueOf(y3);
        System.out.println(s.substring(3));
    }

    public void question4() {
        System.out.println("------CAU 4----------");
        int a, b;
        Scanner scanner = new Scanner(System.in);
        System.out.println("nhap vao so thu nhat: ");
        a = scanner.nextInt();
        System.out.println("moi ban nhap vao so thu hai: ");
        b = scanner.nextInt();
        System.out.println("thuong 2 so vua nhap la: " + (float) a / (float) b);
        scanner.close();

    }


}
