package JAVA_Assignment3;

public class Exercise3 {
    public static void main(String[] args) {
        Exercise3 exercise3 = new Exercise3();
//        exercise3.question1();
//        exercise3.question2();
        exercise3.question3();
    }

    public void question1(){
        Integer a = new Integer(5000);
        float luong = a.intValue();
        System.out.println(luong);
    }

    public void question2(){
        String a = "1234567";
        int a1 = Integer.valueOf(a);
        int a2 = Integer.parseInt(a);
        int a3 = new Integer(a);
        System.out.println(a1);
    }

    public void question3(){
        Integer b = new Integer("1234567");
        int b1 = b.intValue();
        System.out.println(b1);
    }
}
