package utils;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcUtils {
    static Connection connection = null;

    public static void main(String[] args) {
        JdbcUtils.getConnection();
    }

    public static Connection getConnection() {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("TestingSystem_Assignment_10/src/main/resources/db.properties"));

            String userName = properties.getProperty("username");
            String password = properties.getProperty("password");
            String url = properties.getProperty("url");
            String driver = properties.getProperty("driver");

            Class.forName(driver);
            connection = DriverManager.getConnection(url, userName, password);
            if (connection != null) {
//                System.out.println("Thanh Cong");
            } else {
                System.out.println("That Bai");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return connection;
    }
    public static void closeConnection(){
        if (connection != null){
            try{
                connection.close();
            }catch (SQLException e){
                System.err.println("Co loi xay ra");
            }
        }
    }

}
