package frontend;

import entity.Role;
import entity.User1;

import java.util.Scanner;

public class Program1 {
    //    Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Function1 function1 = new Function1();

        System.out.println("Menu---------------");

        while (true) {
            User1 user1 = function1.login();
            if (user1 == null) {
                System.out.println("moi dang nhap lai");
                continue;
            } else if (user1.getRole() == Role.USER) {
                menuUser();

            } else if (user1.getRole() == Role.ADMIN) {
                menuAdmin();
            }
        }
    }

    public static void menuUser() {
        Scanner scanner = new Scanner(System.in);
        Function1 function1 = new Function1();

        while (true) {
//            System.out.println("----".repeat(20));
            System.out.println("--------MenuUser---------");
            System.out.println("Mời bạn chọn chức năng: ");
            System.out.println("1. Hiển thị danh sách tất cả User");
            System.out.println("2. Tìm kiếm User theo ID");
            System.out.println("3. Tìm kiếm User theo username và email");
            System.out.println("4. Hiển thị danh sách tất cả Department");
            System.out.println("5. Tìm kiếm Department theo ID");
            System.out.println("6. Tìm kiếm Department theo DepartmentName");
            System.out.println("7. Log out");
            System.out.println("8, Thoát khỏi chương trình");
            int chose = Integer.parseInt(scanner.nextLine());
            switch (chose) {
                case 1:
                    function1.getAllUser();
                    break;
                case 2:
                    function1.findUserByID();
                    break;
                case 3:
                    function1.findUserByUserNameAndEmail();
                    break;
                case 4:
                    function1.getAllDepartment();
                    break;
                case 5:
                    function1.findDepartmentByID();
                    break;
                case 6:
                    function1.findDepartmentByDepartmentName();
                    break;
                case 7:
                    break;
                case 8:
                    return;
            }
        }
    }

    public static void menuAdmin() {
        Scanner scanner = new Scanner(System.in);
        Function1 function1 = new Function1();

        while (true) {
//            System.out.println("----".repeat(20));
            System.out.println("-----------MenuAdmin-----------");
            System.out.println("Mời bạn chọn chức năng: ");
            System.out.println("1. Hiển thị toàn bộ danh sách User");
            System.out.println("2. Xóa 1 User theo ID");
            System.out.println("3. Thay đổi mật khẩu của 1 User");
            System.out.println("4. Thêm mới 1 User, mặc định password là 123456 và Role là User");
            System.out.println("5. Hiển thị danh sách các Department");
            System.out.println("6. Xóa 1 Department theo ID");
            System.out.println("7, Them moi Department");
            System.out.println("8. Thay đổi tên của 1 department");
            System.out.println("9, Log out");
            System.out.println("10, Thoát chương trình");
            int chose = Integer.parseInt(scanner.nextLine());
            switch (chose) {
                case 1:
                    function1.getAllUser();
                    break;
                case 2:
                    function1.deleteUser();
                    break;
                case 3:
                    function1.updateUser();
                    break;
                case 4:
                    function1.createUser();
                    break;
                case 5:
                    function1.getAllDepartment();
                    break;
                case 6:
                    function1.deleteDepartment();
                    break;
                case 7:
                    function1.createDepartment();
                    break;
                case 8:
                    function1.fixDepartment();
                    break;
                case 9:
                    break;
                case 10:
                    return;

            }
        }
    }
}




