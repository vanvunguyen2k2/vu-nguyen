package frontend;

import backend.controller.AccountController;
import entyti.Account;
import utils.ScanerUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Function {
    AccountController accountController = new AccountController();
    Scanner scanner = new Scanner(System.in);
    List<Account> accounts = new ArrayList<>();
    public void createAccount(){

        System.out.println("moi ban nhap username");
        String name = ScanerUtils.inputString();
        System.out.println("moi ban nhap vao email");
        String email = ScanerUtils.inputEmail();
        System.out.println("moi ban nhap vao password");
        String pass = scanner.nextLine();

        accountController.createAccount(name, email, pass);

    }
    public void updateAccount() {

        System.out.println("moi ban nhap id nguoi dung muon thay doi password ");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.println("moi ban nhap vao password cu ");
        String old_password = scanner.nextLine();
        System.out.println("Moi ban nhap vao password moi");
        String new_password = scanner.nextLine();
        accountController.updateAccount(id, old_password, new_password);

    }
    public void deleteAccount() {

        System.out.println("moi ban nhap vao id cua account ma ban muon xoa");
        int id2 = Integer.parseInt(scanner.nextLine());
        accountController.deleteAccount(id2);

    }
    public void findAccount() {
        System.out.println("Moi ban nhap vao email can tim kiem");
        String email = ScanerUtils.inputString();
        List<Account> accounts = accountController.findAccountbyemail(email);
        String leftAlignFormat = "| %-3s| %-15s | %-17s | %-15s |%n";
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
        System.out.format("| id |     fullName    |       email       |     password    |%n");
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
        for (Account account : accounts) {
            System.out.format(leftAlignFormat, account.getAccountId(), account.getFullname(), account.getEmail(), account.getPassWord());
        }
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
//        example.findAccountbyemail(email);

    }
    public void getAllAccount(){
        List<Account> accounts = accountController.getAllAccount();
        String leftAlignFormat = "| %-3s| %-15s | %-17s | %-15s |%n";
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
        System.out.format("| id |     fullName    |       email       |     password    |%n");
        System.out.format("+----+-----------------+-------------------+-----------------+%n");

        try{
            for (Account account: accounts) {
                System.out.format(leftAlignFormat, account.getAccountId(), account.getFullname(), account.getEmail(), account.getPassWord());
            }
        }catch (NullPointerException e){
            System.err.println(e.getMessage());
        }
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
//        for (Account a:accounts) {
//            System.out.println(accounts);
//            break;
//        }

    }

    public void login() {
        System.out.println("moi ban nhap vao email");
        String email = ScanerUtils.inputEmail();
        System.out.println("moi ban nhap vao password");
        String password = scanner.nextLine();
        if (accountController.login(email, password)) {
            System.out.println("dang nhap thanh cong");
        } else {
            System.out.println("dang nhap that bai");
        }
    }
}
