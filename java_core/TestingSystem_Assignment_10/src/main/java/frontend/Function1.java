package frontend;

import backend.controller.Department1Controller;
import backend.controller.UserController;
import entity.Department1;
import entity.User1;
import utils.ScanerUtils;

import java.util.List;
import java.util.Scanner;

public class Function1 {
    Scanner scanner = new Scanner(System.in);
    Department1Controller department1Controller = new Department1Controller();
    UserController userController = new UserController();

    public User1 login() {


        System.out.println("moi ban nhap vao username");
        String user = scanner.nextLine();
        System.out.println("moi ban nhap vao password");
        int password = ScanerUtils.inputNumber();
        return userController.login(user, password);
//            if (department1Controller.login(user, password)) {
//                System.out.println("dang nhap thanh cong");
//            } else {
//                System.out.println("dang nhap that bai");
//            }
    }

    public void getAllUser() {
        List<User1> userList = userController.getAllUser();
        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+-----------------+%n");
        System.out.format("| id |   role   |     userName    |        email      | date_of_birth   | department_name | password        |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+-----------------+%n");
        for (User1 user : userList) {
            System.out.format(leftAlignFormat, user.getId(), user.getRole().name(), user.getUser_name(), user.getEmail(),
                    user.getDate_of_birth(), user.getDepartment1().getDepartmentName(), user.getPassword());
        }
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+-----------------+%n");
    }

    public void findUserByID() {

        System.out.println("Moi ban nhap vao id can tim kiem");
        int id = Integer.parseInt(scanner.nextLine());
//        List<User1> userList = userController.findUserByID(id);
        User1 user1 = userController.findUserByID(id);

//        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
//        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+-----------------+%n");
//        System.out.format("| id |   role   |     userName    |        email      | date_of_birth   | department_name | password        |%n");
//        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+-----------------+%n");
////        for (User1 user : userController.getAllUser()) {
//            System.out.format(leftAlignFormat, user.getId(), user.getRole().name(), user.getUser_name(), user.getEmail(),
//                    user.getDate_of_birth(), user.getDepartment1().getDepartmentName(), user.getPassword());
//        }
        if (user1 != null) {
            String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%-15s|%n";
            System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+-----------------+%n");
            System.out.format("| id |   role   |     userName    |        email      | date_of_birth   | department_name | password        |%n");
            System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+-----------------+%n");
            System.out.format(leftAlignFormat, user1.getId(), user1.getRole().name(), user1.getUser_name(), user1.getEmail(),
                    user1.getDate_of_birth(), user1.getDepartment1().getDepartmentName(), user1.getPassword());
        } else {
            System.out.println("khong ton tai");

        }

        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+-----------------+%n");
    }

    public void findUserByUserNameAndEmail() {
        System.out.println("Moi ban nhap vao key");
        String key = scanner.nextLine();
        List<User1> user1List = userController.findUserByUserNameAndEmail(key);

        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+-----------------+%n");
        System.out.format("| id |   role   |     userName    |        email      | date_of_birth   | department_name | password        |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+-----------------+%n");
        for (User1 user : user1List) {
            System.out.format(leftAlignFormat, user.getId(), user.getRole().name(), user.getUser_name(), user.getEmail(),
                    user.getDate_of_birth(), user.getDepartment1().getDepartmentName(), user.getPassword());
        }
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+-----------------+%n");
    }

    public void getAllDepartment() {
        List<Department1> department1s = department1Controller.getAllDepartment();
        String leftAlignFormat = "| %-3s| %-9s |%n";
        System.out.format("+----+----------+---------------+%n");
        System.out.format("| id |     DepartmentName       |%n");
        System.out.format("+----+----------+---------------+%n");
        for (Department1 department1 : department1s) {
            System.out.format(leftAlignFormat, department1.getDepartmentID(), department1.getDepartmentName());
        }
        System.out.format("+----+----------+---------------+%n");
    }


    public void findDepartmentByID() {

        System.out.println("Moi ban nhap vao id can tim kiem");
        int id = Integer.parseInt(scanner.nextLine());
//        List<Department1> department1s = department1Controller.findDepartmentByID(id);
        Department1 department1 = department1Controller.findDepartmentByID(id);
        String leftAlignFormat = "| %-3s| %-8s |%n";
        System.out.format("+----+----------+-------------+%n");
        System.out.format("| id |     DepartmentName     |%n");
        System.out.format("+----+----------+-------------+%n");
//        for (Department1 department1 : department1s) {
        System.out.format(leftAlignFormat, department1.getDepartmentID(), department1.getDepartmentName());
//        }
        System.out.format("+----+----------+-------------+%n");
    }

    public void findDepartmentByDepartmentName() {
        System.out.println("Moi ban nhap vao department_name");
        String department_name = scanner.nextLine();
        List<Department1> department1s = department1Controller.findDepartmentByDepartmentName(department_name);

        String leftAlignFormat = "| %-3s| %-8s |%n";
        System.out.format("+----+----------+------------+%n");
        System.out.format("| id |     DepartmentName    |%n");
        System.out.format("+----+----------+------------+%n");
        try {
            for (Department1 department1 : department1s) {
                System.out.format(leftAlignFormat, department1.getDepartmentID(), department1.getDepartmentName());
            }
            System.out.format("+----+----------+------------+%n");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void createUser() {

        System.out.println("Mời bạn nhập vào tên của User mới");
        String nameOfNewUser = scanner.nextLine();
        System.out.println("Mời bạn nhập vào Email mới");
        String email = ScanerUtils.inputEmail();
        System.out.println("moi ban nhap ngay");
        String day = ScanerUtils.inputString();
        System.out.println("moi ban chon phong ban");
        System.out.println("1, java");
        System.out.println("2, php");
        System.out.println("3, scrummaster");

        int departmentID = ScanerUtils.inputNumber(1, 3);
        userController.createUser(nameOfNewUser, email, day, departmentID);


    }
//

    public void updateUser() {
        System.out.println("Mời bạn nhập vào id của User cần thay đổi password");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.println("Mời bạn nhập vào password cũ");
        int old_password = Integer.parseInt(scanner.nextLine());
        System.out.println("Mời bạn nhập vào password mới");
        int new_password = Integer.parseInt(scanner.nextLine());
        userController.updateUser(id, old_password, new_password);
    }

    public void deleteUser() {
        System.out.println("Mời bạn nhập vào id của User cần xóa");
        int id = Integer.parseInt(scanner.nextLine());

        User1 user1 = userController.findUserByID(id);
        if (user1 == null) {
            System.out.println("User id chua ton tai");
        } else {
            userController.deleteUser(id);
        }
    }

    public void deleteDepartment() {
        System.out.println("Mời bạn nhập vào id của Department cần xóa");
        int id = Integer.parseInt(scanner.nextLine());

        Department1 department1 = department1Controller.findDepartmentByID(id);
        if (department1 == null) {
            System.out.println("Department_id chua ton tai");
        } else {
            department1Controller.deleteDepartment(id);
        }
    }

    public void createDepartment() {
        System.out.println("Moi ban nhap vao id cua phong ban moi");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.println("Moi ban nhap vao ten phong ban");
        String department_name = scanner.nextLine();
        department1Controller.createDepartment(id, department_name);
    }

    public void fixDepartment(){
        System.out.println("Moi ban nhap vao id cua phong ban can sua ten");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.println("Moi ban nhap vao ten phong ban moi");
        String name = scanner.nextLine();
        department1Controller.fixDepartmentName(id, name);
    }
}






