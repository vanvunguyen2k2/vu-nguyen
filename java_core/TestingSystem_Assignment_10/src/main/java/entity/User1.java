package entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class User1 {
    private int id;
    private Role role;
    private String user_name;
    private int password;
    private String email;
    private String date_of_birth;
    private Department1 department1;

    public User1() {
    }

    public User1(int id, Role role, String user_name, int password, String email, String date_of_birth, Department1 department1, String department_name) {
        this.id = id;
        this.role = Role.USER;
        this.user_name = user_name;
        this.password = password;
        this.email = email;
        this.date_of_birth = date_of_birth;
        this.department1 = department1;
    }


}
