package backend.controller;

import backend.service.AccountService;
import entyti.Account;

import java.util.List;

public class AccountController {
    AccountService accountService = new AccountService();

    public void createAccount(String username, String email, String password) {
        accountService.createAccount(username, email, password);
    }

    public void updateAccount(int id, String old_password, String new_password) {
        accountService.updateAccount(id, old_password, new_password);
    }

    public void deleteAccount(int id2) {
        accountService.deleteAccount(id2);
    }

    public List<Account> findAccountbyemail(String email){
        accountService.findAccountbyemail(email);
        return null;
    }

    public List<Account> getAllAccount(){
        accountService.getAllAccount();
        return null;
    }

    public boolean login(String email, String password){
        accountService.login(email, password);
        return true;
    }
}
