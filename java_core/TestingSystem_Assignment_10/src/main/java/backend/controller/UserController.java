package backend.controller;

import backend.service.UserService;
import entity.Role;
import entity.User1;

import java.util.List;

public class UserController {

    UserService userService = new UserService();

    public User1 login(String user_name, int password) {
        return userService.login(user_name, password);
    }

    public List<User1> getAllUser() {
        return userService.getAllUser();
    }

    public User1 findUserByID(int id) {
        return userService.findUserByID(id);
    }

    public List<User1> findUserByUserNameAndEmail(String key) {
        return userService.findUserByUserNameAndEmail(key);
    }

    public void createUser(String user_name, String email, String date_of_birth, int department_id) {
        userService.createUser(user_name, email, date_of_birth, department_id);
    }

    public void updateUser(int id, int old_password, int new_password) {
        userService.updateUser(id, old_password, new_password);
    }

    public void deleteUser(int id) {
        userService.deleteUser(id);
    }
}
