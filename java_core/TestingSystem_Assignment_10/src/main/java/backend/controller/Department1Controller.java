package backend.controller;

import backend.service.Department1Service;
import entity.Department1;

import java.util.List;

public class Department1Controller {
    Department1Service department1Service = new Department1Service();

    public Department1 login(String user, String password) {
        return department1Service.login(user, password);
    }

    public List<Department1> getAllDepartment() {
        return department1Service.getAllDepartment();
    }

    public Department1 findDepartmentByID(int id) {
        return department1Service.findDepartmentByID(id);
    }

    public List<Department1> findDepartmentByDepartmentName(String department_name) {
        return department1Service.findDepartmentByDepartmentName(department_name);
    }

    public void deleteDepartment(int id) {
        department1Service.deleteDepartment(id);
    }

    public void createDepartment(int department_id, String department_name){
        department1Service.createDepartment(department_id, department_name);
    }

    public void fixDepartmentName(int department_id, String department_name){
        department1Service.fixDepartmentName(department_id, department_name);
    }
}
