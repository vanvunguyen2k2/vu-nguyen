package backend;

import entyti.Account;
import utils.JdbcUtils;
import utils.ScanerUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Example {
    public Example() {
    }
    Scanner scanner = new Scanner(System.in);
    public void createAccount(String username, String email, String password){

        // tao 1 cau query tuong ung voi chuc nang muon su dung

        String sql = "INSERT INTO JDBC.ACCOUNT (full_name, email, password) VALUES(?, ?, ?)  ";
        // Ket noi toi database de tao 1 phien lam viec
        Connection connection = JdbcUtils.getConnection();
        // tao statement tuong ung voi cau query bien chuyen vao parameterStatement

        // khong co bien chuyen vao : statement
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            // khong co bien chuyen vao : statement
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, password);

            // Execute cau query va lay ket qua
            int resultSet = preparedStatement.executeUpdate();


            // Kiem tra su thanh cong va thong bao

            if (resultSet == 0){
                System.out.println("them moi that bai");
            }else {
                System.out.println("them moi thanh cong");
            }
            JdbcUtils.closeConnection();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        // tao 1 cau query tuong ung voi chuc nang muon su dung

        // Execute cau query va lay ket qua

        // Kiem tra su thanh cong va thong bao



    }

    public List<Account> getAllAccount(){
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM JDBC.ACCOUNT;";
        //
        Connection connection = JdbcUtils.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while ( resultSet.next()){
                // lay gia tri tung hang gan vao doi tuong account tuong ung
                Account account = new Account();
                int accountid = resultSet.getInt("account_id");
                account.setAccountId(accountid);
                account.setFullname(resultSet.getString("full_name"));
                account.setEmail(resultSet.getString("email"));
                account.setPassWord(resultSet.getString("password"));

                accounts.add(account);
            }


        } catch (SQLException e) {
            throw  new RuntimeException(e);

        }

        return accounts;

    }


    public void updateAccount(int id, String name, String email, String password){
        String sql = "UPDATE ACCOUNT SET full_name = ? WHERE (account_id = ?); ";
//        String sql1 = "UPDATE ACCOUNT SET ACCOUNT_ID = ? WHERE FULL_NAME = ?; ";


        Connection connection = JdbcUtils.getConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, id);
        }catch (SQLException e){
            System.err.println(e.getMessage());
        }

    }
    public void updateAccount(String ten, int id){
    }

    public void deleteAccount(int id2){
        String sql = "DELETE FROM JDBC.ACCOUNT WHERE account_id = " + id2;

        // TAO RA CONNECTION

        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            int results = preparedStatement.executeUpdate();
//            if(results == 0){
//                System.out.println("Xoa thanh cong");
//            }else {
//                System.out.println("That bai");
//            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }


    }
public List<Account> findAccout(String email){
         List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM ACCOUNT WHERE EMAIL = 'email'; ";

    // Tao connection đến sql


        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet re = preparedStatement.executeQuery();
            for (Account ac: accounts) {
                if (ac.getEmail().equals(re)){
                    System.out.println(ac);
                }

            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return accounts;
    }

    public void login(){

    }
}
