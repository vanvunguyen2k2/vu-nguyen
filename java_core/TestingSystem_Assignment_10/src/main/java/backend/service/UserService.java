package backend.service;

import backend.repository.UserRepository;
import entity.Role;
import entity.User1;

import java.util.List;

public class UserService implements IUserService {

    private UserRepository userRepository = new UserRepository();


    @Override
    public User1 login(String username, int password) {
        return userRepository.login(username, password);
    }

    @Override
    public List<User1> getAllUser() {
        return userRepository.getAllUser();
    }

    @Override
    public User1 findUserByID(int id) {
        return userRepository.findUserByID(id);
    }

    @Override
    public List<User1> findUserByUserNameAndEmail(String key) {
        return userRepository.findUserByUserNameAndEmail(key);
    }



    public void createUser(String user_name, String email, String date_of_birth, int department_id) {
         userRepository.createUser(user_name, email, date_of_birth, department_id);
    }

    @Override
    public void updateUser(int id, int old_password, int new_password) {
        userRepository.updateUser(id, old_password, new_password);
    }

    public void deleteUser(int id) {
        userRepository.deleteUser(id);
    }
}
