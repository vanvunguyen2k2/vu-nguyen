package backend.service;

import backend.repository.DepartmentRepository1;
import entity.Department1;

import java.util.List;

public class Department1Service implements IDpartment1Service{

    private DepartmentRepository1 departmentRepository1 = new DepartmentRepository1();

    @Override
    public Department1 login(String user, String password) {
        return departmentRepository1.login(user, password);
    }

    @Override
    public List<Department1> getAllDepartment() {
        return departmentRepository1.getAllDepartment();
    }

    public Department1 findDepartmentByID(int id){
        return departmentRepository1.findDepartmentByID(id);
    }

    public List<Department1> findDepartmentByDepartmentName(String department_name){
        return departmentRepository1.findDepartmentByDepartmentName(department_name);
    }

    @Override
    public void deleteDepartment(int id) {
        departmentRepository1.deleteDepartmentByDepartmentID(id);
    }

    public void createDepartment(int department_id, String department_name){
        departmentRepository1.createDepartment(department_id, department_name);
    }

    @Override
    public void fixDepartmentName(int department_id, String department_name) {
        departmentRepository1.fixDepartmentName(department_id, department_name);
    }

}
