package backend.service;

import backend.repository.AccountRepository;
import entyti.Account;

import java.util.List;

public class AccountService implements IAccountService{

    private AccountRepository accountRepository = new AccountRepository();

    @Override
    public void createAccount(String username, String email, String password) {
        accountRepository.createAccount(username, email, password);
    }

    @Override
    public void updateAccount(int id, String old_password, String new_password) {
        accountRepository.updateAccount(id, old_password, new_password);
    }

    @Override
    public void deleteAccount(int id2) {
        accountRepository.deleteAccount(id2);
    }

    @Override
    public List<Account> findAccountbyemail(String email) {
        accountRepository.findAccountbyemail(email);
        return null;
    }

    @Override
    public void getAllAccount() {
        accountRepository.getAllAccount();


    }

    @Override
    public boolean login(String email, String password) {
        accountRepository.login(email, password);

        return true;
    }
}
