package backend.service;

import entity.Role;
import entity.User1;

import java.util.List;

public interface IUserService {

    User1 login(String username, int password);

    List<User1> getAllUser();

    User1 findUserByID(int id);

    List<User1> findUserByUserNameAndEmail(String key);

    void createUser(String user_name, String email, String date_of_birth, int department_id);

    void updateUser(int id, int old_password, int new_password);


}
