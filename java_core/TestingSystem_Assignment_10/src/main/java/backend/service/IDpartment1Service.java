package backend.service;

import entity.Department1;

import java.util.List;

public interface IDpartment1Service {

    Department1 login(String user, String password);

    List<Department1> getAllDepartment();

    Department1 findDepartmentByID(int id);

    List<Department1> findDepartmentByDepartmentName(String department_name);

    void deleteDepartment(int id);

    void createDepartment(int department_id, String department_name);

    void fixDepartmentName(int department_id, String department_name);


}

