package backend.service;

import entyti.Account;

import java.util.List;

public interface IAccountService {

    void createAccount(String username, String email, String password);

    void updateAccount(int id, String old_password, String new_password);

    void deleteAccount(int id2);

    List<Account> findAccountbyemail(String email);

    void getAllAccount();

    boolean login(String email, String password);

}
