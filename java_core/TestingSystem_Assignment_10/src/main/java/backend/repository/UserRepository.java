package backend.repository;

import entity.Department1;
import entity.Role;
import entity.User1;
import utils.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {


    // METHOD DÙNG ĐỂ ĐĂNG NHẬP VÀO USER
    public User1 login(String username, int password) {
        String sql = "SELECT * FROM ASSIGNMENT10.USER WHERE USER_NAME = ? AND PASSWORD = ?;";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setInt(2, password);
            ResultSet re = preparedStatement.executeQuery();

            if (re.next()) {
                User1 user1 = new User1();
                user1.setUser_name(username);
                user1.setEmail(re.getString("email"));
                String role_string = re.getString("role"); // 1, Admin;  2, User
                user1.setRole(Role.valueOf(role_string));
                return user1;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());

        } finally {
            JdbcUtils.closeConnection();
        }
        return null;

    }

    // MEHTHOD DÙNG ĐỂ HIỂN THỊ DANH SÁCH TẤT CẢ CÁC USER
    public List<User1> getAllUser() {
        List<User1> user1List = new ArrayList<>();
        String sql = "SELECT * FROM USER LEFT JOIN DEPARTMENT D ON D.DEPARTMENT_ID = USER.DEPARTMENT_ID";
        Connection connection = JdbcUtils.getConnection();

        try {
            Statement statement = connection.createStatement();
            ResultSet re = statement.executeQuery(sql);
            while (re.next()) {
                User1 user1 = new User1();
                Department1 department1 = new Department1();

                user1.setId(re.getInt("id"));
                user1.setUser_name(re.getString("user_name"));
                user1.setPassword(re.getInt("password"));
                user1.setRole(Role.valueOf(re.getString("role")));
                user1.setEmail(re.getString("email"));
                user1.setDate_of_birth(re.getString("date_of_birth"));
                department1.setDepartmentName(re.getString("department_name"));

                user1.setDepartment1(department1);
                user1List.add(user1);

            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return user1List;
    }

    // METHOD DÙNG ĐỂ TÌM KIẾM USER THEO ID
    public User1 findUserByID(int id) {
//        List<User1> user1List = new ArrayList<>();
        String sql = "SELECT * FROM USER U left join Department D on D.department_id = U.Department_id WHERE ID = ?";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement pr = connection.prepareStatement(sql);
            pr.setInt(1, id);
            ResultSet re = pr.executeQuery();
            while (re.next()) {
                User1 user1 = new User1();
                Department1 department1 = new Department1();
                user1.setUser_name(re.getString("user_name"));
                user1.setId(re.getInt("id"));
                user1.setRole(Role.valueOf(re.getString("role")));
                user1.setPassword(re.getInt("password"));
                user1.setEmail(re.getString("email"));
                department1.setDepartmentName(re.getString("department_name"));                user1.setDepartment1(department1);
                return user1;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }

        return null;
    }

    // DÙNG METHOD ĐỂ TÌM KIẾM USER THEO USERNAME VÀ EMAIL

    public List<User1> findUserByUserNameAndEmail(String key) {
        List<User1> user1List = new ArrayList<>();
        key = "%" + key + "%";
        String sql = "SELECT * FROM USER U left join Department D on D.department_id = U.Department_id WHERE U.USER_NAME like ? OR U.EMAIL like ?";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement pr = connection.prepareStatement(sql);
            pr.setString(1, key);
            pr.setString(2, key);


            ResultSet re = pr.executeQuery();
            while (re.next()) {
                User1 user1 = new User1();
                Department1 department1 = new Department1();
                user1.setUser_name(re.getString("user_name"));
                user1.setEmail(re.getString("email"));
                user1.setId(re.getInt("id"));
                user1.setRole(Role.valueOf(re.getString("role")));
                user1.setPassword(re.getInt("password"));
                department1.setDepartmentName(re.getString("department_name"));                user1.setDepartment1(department1);
                user1List.add(user1);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return user1List;
    }

    public void createUser(String user_name, String email, String date_of_birth, int department_id) {
//        List<User1> user1List = new ArrayList<>();
        String sql = "INSERT INTO USER (ROLE, USER_NAME, PASSWORD, EMAIL, date_of_birth, department_id) VALUES('USER', ?, '123456', ?, ?, ?)";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement pr = connection.prepareStatement(sql);
            Department1 department11 = new Department1();
            pr.setString(1, user_name);

            pr.setString(2, email);
            pr.setString(3, date_of_birth);
            pr.setInt(4, department_id);


            int re = pr.executeUpdate();

            if (re == 0) {
                System.out.println("Thêm mới User không thành công");
            } else {
                System.out.println("Thêm mới thành công");
            }


        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    public void updateUser(int id, int old_password, int new_password) {
        String sql = "UPDATE USER SET PASSWORD = ? WHERE ID = ? AND PASSWORD = ?";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement pr = connection.prepareStatement(sql);
            pr.setInt(1, new_password);
            pr.setInt(2, id);
            pr.setInt(3, old_password);

            int re = pr.executeUpdate();

            if (re == 0) {
                System.out.println("Sửa không thành công");
            } else {
                System.out.println("Sửa thành công");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void deleteUser(int id) {
        String sql = "DELETE FROM USER WHERE ID = ?";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement pr = connection.prepareStatement(sql);
            pr.setInt(1, id);

            int rs = pr.executeUpdate();

            if (rs == 0) {
                System.out.println("Khong thanh cong");
            } else {
                System.out.println("thanh cong");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    //  Dùng method để Xóa 1 department theo Id  ( có kiểm tra nếu Id không tồn tại thì in ra thông báo)
    public void deleteDepartment(int department_id) {
        String sql = "DELETE FROM DEPARTMENT WHERE DEPARTMENT_ID = ?";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement pr = connection.prepareStatement(sql);
            pr.setInt(1, department_id);

            int rs = pr.executeUpdate();

            if (rs == 0) {
                System.out.println("Khong thanh cong");
            } else {
                System.out.println("thanh cong");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


}
