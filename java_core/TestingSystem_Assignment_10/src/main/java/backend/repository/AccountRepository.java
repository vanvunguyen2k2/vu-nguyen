package backend.repository;

import backend.controller.AccountController;
import entyti.Account;
import utils.JdbcUtils;
import utils.ScanerUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AccountRepository {

    public void createAccount(String username, String email, String password){

        // tao 1 cau query tuong ung voi chuc nang muon su dung

        String sql = "INSERT INTO JDBC.ACCOUNT (full_name, email, password) VALUES(?, ?, ?)  ";
        // Ket noi toi database de tao 1 phien lam viec
        Connection connection = JdbcUtils.getConnection();
        // tao statement tuong ung voi cau query bien chuyen vao parameterStatement

        // khong co bien chuyen vao : statement
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            // khong co bien chuyen vao : statement
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, password);

            // Execute cau query va lay ket qua
            int resultSet = preparedStatement.executeUpdate();


            // Kiem tra su thanh cong va thong bao

            if (resultSet == 0){
                System.out.println("them moi that bai");
            }else {
                System.out.println("them moi thanh cong");
            }
            JdbcUtils.closeConnection();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        // tao 1 cau query tuong ung voi chuc nang muon su dung

        // Execute cau query va lay ket qua

        // Kiem tra su thanh cong va thong bao



    }

    public void updateAccount(int id, String old_password, String new_password){
        String sql = "UPDATE JDBC.ACCOUNT SET password = ? WHERE account_id = ? and password = ?; ";
//        String sql1 = "UPDATE ACCOUNT SET ACCOUNT_ID = ? WHERE FULL_NAME = ?; ";




        try{
            Connection connection = JdbcUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, new_password);
            preparedStatement.setInt(2, id);
            preparedStatement.setString(3, old_password);

            int rs = preparedStatement.executeUpdate();
            if (rs == 0){
                System.out.println("khong thay doi duoc");

            }else {
                System.out.println("thay doi thanh cong");
            }
            JdbcUtils.closeConnection();

        }catch (SQLException e){
            System.err.println(e.getMessage());
        }

    }

    public void deleteAccount(int id2){
        String sql = "DELETE FROM JDBC.ACCOUNT WHERE account_id = " + id2;

        // TAO RA CONNECTION

        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            int results = preparedStatement.executeUpdate();
            if(results == 0){
                System.out.println("Khong thanh cong");
            }else {
                System.out.println("Thanh cong");
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }finally {
            JdbcUtils.closeConnection();
        }

    }

    public List<Account> findAccountbyemail(String email) {
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM JDBC.ACCOUNT WHERE EMAIL LIKE ?; ";
        String word = "%" + email + "%";

        // Tao connection đến sql


        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, word);
            ResultSet re = preparedStatement.executeQuery();
            while (re.next()) {
                Account account = new Account();
//                int accountID = re.getInt("account_id"); // nhu dong duoi
                account.setAccountId(re.getInt("account_id"));
                account.setFullname(re.getString("full_name"));
                account.setEmail(re.getString("email"));
                account.setPassWord(re.getString("password"));
                accounts.add(account);

            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return accounts;
    }


    public List<Account> getAllAccount(){
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM JDBC.ACCOUNT;";
        //
        Connection connection = JdbcUtils.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while ( resultSet.next()){
                // lay gia tri tung hang gan vao doi tuong account tuong ung
                Account account = new Account();
                int accountid = resultSet.getInt("account_id");
                account.setAccountId(accountid);
                account.setFullname(resultSet.getString("full_name"));
                account.setEmail(resultSet.getString("email"));
                account.setPassWord(resultSet.getString("password"));

                accounts.add(account);
            }
            JdbcUtils.closeConnection();


        } catch (SQLException e) {
            throw new RuntimeException(e);

        }

        return accounts;

    }

    public boolean login(String email, String password) {
        String sql = "SELECT * FROM JDBC.ACCOUNT WHERE EMAIL = ? AND PASSWORD = ?;";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet re = preparedStatement.executeQuery();

            return re.next();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        } finally {
            JdbcUtils.closeConnection();
        }
    }
}
