package backend.repository;

import entity.Department1;
import utils.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DepartmentRepository1 {
    public Department1 login(String email, String password) {
        String sql = "SELECT * FROM DEPARTMENT WHERE EMAIL = ? AND PASSWORD = ?;";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet re = preparedStatement.executeQuery();

            while (re.next()) {
                Department1 department1 = new Department1();
                department1.setDepartmentID(re.getInt("department_id"));
                department1.setDepartmentName(re.getString("department_name"));
                return department1;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());

        } finally {
            JdbcUtils.closeConnection();
        }
        return null;
    }

    // DÙNG METHOD ĐỂ HIỂN THỊ DANH SÁCH TẤT CẢ CÁC DEPARTMENT ( IN RA DẠNG BẢNG )
    public List<Department1> getAllDepartment() {
        List<Department1> department1s = new ArrayList<>();
        String sql = "SELECT * FROM DEPARTMENT";
        Connection connection = JdbcUtils.getConnection();

        try {
            Statement st = connection.createStatement();
            ResultSet re = st.executeQuery(sql);
            while (re.next()) {
                Department1 department1 = new Department1();
                department1.setDepartmentID(re.getInt("department_id"));
                department1.setDepartmentName(re.getString("department_name"));
                department1s.add(department1);
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return department1s;
    }


    // METHOD DÙNG ĐỂ TÌM KIẾM USER THEO ID
    public Department1 findDepartmentByID(int id) {
//        List<Department1> department1s = new ArrayList<>();

        // tao cau sql
        String sql = "SELECT * FROM DEPARTMENT WHERE DEPARTMENT_ID = ?";

        // tao connection
        Connection connection = JdbcUtils.getConnection();


        try {
            PreparedStatement pr = connection.prepareStatement(sql);
            pr.setInt(1, id);
            ResultSet re = pr.executeQuery();
            while (re.next()) {
//                User1 user1 = new User1();
                Department1 department1 = new Department1();

                department1.setDepartmentName(re.getString("department_name"));
                department1.setDepartmentID(re.getInt("department_id"));
//                user1.setUser_name(re.getString("user_name"));
//                user1.setId(re.getInt("id"));
//                user1.setRole(Role.valueOf(re.getString("role")));
//                user1.setPassword(re.getInt("password"));
//                user1.setEmail(re.getString("email"));
                return department1;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return null;
    }

    public List<Department1> findDepartmentByDepartmentName(String department_name) {
        List<Department1> department1s = new ArrayList<>();
        String sql = "SELECT * FROM DEPARTMENT WHERE DEPARTMENT_NAME = ?";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement pr = connection.prepareStatement(sql);
            pr.setString(1, department_name);

            ResultSet re = pr.executeQuery();
            while (re.next()) {
                Department1 department1 = new Department1();
                department1.setDepartmentID(re.getInt("department_id"));
                department1.setDepartmentName(re.getString("department_name"));
                department1s.add(department1);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return department1s;
    }

    public void deleteDepartmentByDepartmentID(int department_id) {
        // tao 1 cau sql
        String sql = "DELETE FROM DEPARTMENT WHERE DEPARTMENT_ID = ?";

        // tao ket noi
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement pr = connection.prepareStatement(sql);
            pr.setInt(1, department_id);
            int re = pr.executeUpdate();
            if (re == 0) {
                System.out.println("khong thanh cong");
            } else {
                System.out.println("thanh cong");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void createDepartment(int department_id, String department_name) {

        String sql = "INSERT INTO DEPARTMENT(DEPARTMENT_ID, DEPARTMENT_NAME) VALUES (?, ?)";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement pr = connection.prepareStatement(sql);
            pr.setInt(1, department_id);
            pr.setString(2, department_name);
            int re = pr.executeUpdate();

            if (re == 0) {
                System.out.println("khong them moi thanh cong moi ban thu lai");
            } else {
                System.out.println("them moi thanh cong");
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void fixDepartmentName(int department_id, String department_name) {
        String sql = "UPDATE DEPARTMENT SET DEPARTMENT_NAME = ? WHERE DEPARTMENT_ID = ?";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement pr = connection.prepareStatement(sql);


            pr.setString(1, department_name);
            pr.setInt(2, department_id);
            int rs = pr.executeUpdate();

            if (rs == 0) {
                System.out.println("khong thanh cong moi ban thu lai");
            } else {
                System.out.println(" thanh cong ");
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }
}
