package TestingSystem_Assignment_1;

import org.w3c.dom.ls.LSOutput;

import java.time.LocalDate;
import java.util.Date;

public class Program {
    public static void main(String[] args) {
        //  Tao department
        System.out.println("them gia tri cho Department");
        Department department1 = new Department();
        department1.departmentID = 1;
        department1.departmentName = "Sale";

        System.out.println(department1.departmentName);
        System.out.println(department1.departmentID);

        Department department2 = new Department();
        department2.departmentID = 2;
        department2.departmentName = "Marketing";

        System.out.println(department2.departmentName);
        System.out.println(department2.departmentID);

        Department department3 = new Department();
        department3.departmentID = 3;
        department3.departmentName = "Service";

        System.out.println(department3.departmentName);
        System.out.println(department3.departmentID);

        Department department4 = new Department();
        department4.departmentID = 4;
        department4.departmentName = "Vip";

        System.out.println(department4.departmentName);
        System.out.println(department4.departmentID);

        // tao group
        Group group11 = new Group();
        Group group21 = new Group();
        Group group31 = new Group();
        Group group41 = new Group();
        Group[] arrgroup1 = {group21, group11, group31, group41};

        // Tao position
        System.out.println("them gia tri cho position");
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = "Dev";

        System.out.println(position1.positionId);
        System.out.println(position1.positionName);

        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = "Test";

        System.out.println(position2.positionId);
        System.out.println(position2.positionName);

        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = "Scrum Master";

        System.out.println(position3.positionId);
        System.out.println(position3.positionName);

        Position position4 = new Position();
        position4.positionId = 4;
        position4.positionName = "PM";

        System.out.println(position4.positionId);
        System.out.println(position4.positionName);

        //   Tao Account
        System.out.println("them gia tri cho Account");
        Account account1 = new Account();
        account1.accountId = 1;
        account1.email = "vunguyen@gmail.com";
        account1.userName = "Vu";
        account1.fullName = "Nguyen Van Vu";
        account1.department = department1;
        account1.position = position1;
        account1.createDate = new Date();

        System.out.println(account1.accountId);
        System.out.println(account1.email);
        System.out.println(account1.fullName);
        System.out.println(account1.department);
        System.out.println(account1.position);

        Account account2 = new Account();
        account2.accountId = 2;
        account2.groups = arrgroup1;
        account2.email = "vannguyen@gmail.com";
        account2.userName = "Van";
        account2.fullName = "Nguyen Thi Van";
        account2.department = department2;
        account2.position = position2;
        account2.createDate = new Date();

        System.out.println(account2.accountId);
        System.out.println(account2.email);
        System.out.println(account2.fullName);
        System.out.println(account2.department);
        System.out.println(account2.position);

        Account account3 = new Account();
        account3.accountId = 3;
        account3.email = "vinhnguyen@gmail.com";
        account3.userName = "Vinh";
        account3.fullName = "Nguyen Van Vinh";
        account3.department = department3;
        account3.position = position3;
        account3.createDate = new Date();

        System.out.println(account3.accountId);
        System.out.println(account3.email);
        System.out.println(account3.fullName);
        System.out.println(account3.department);
        System.out.println(account3.position);

        Account account4 = new Account();
        account4.accountId = 4;
        account4.email = "hoangnguyen@gmail.com";
        account4.userName = "Hoang";
        account4.fullName = "Nguyen Van Hoang";
        account4.department = department4;
        account4.position = position4;
        account4.createDate = new Date();

        System.out.println(account4.accountId);
        System.out.println(account4.email);
        System.out.println(account4.fullName);
        System.out.println(account4.department);
        System.out.println(account4.position);
        //   Tao Group
        System.out.println("them gia tri cho Group");
        Group group1 = new Group();
        group1.groupId = 1;
        group1.groupName = "Nhom 1";
        group1.creator = account1;
        group1.createDate = new Date();

        System.out.println(group1.groupId);
        System.out.println(group1.groupName);
        System.out.println(group1.creator);

        Group group2 = new Group();
        group2.groupId = 2;
        group2.groupName = "Nhom 2";
        group2.creator = account2;
        group2.createDate = new Date();

        System.out.println(group2.groupId);
        System.out.println(group2.groupName);
        System.out.println(group2.creator);

        Group group3 = new Group();
        group3.groupId = 3;
        group3.groupName = "Nhom 3";
        group3.creator = account3;
        group3.createDate = new Date();

        System.out.println(group3.groupId);
        System.out.println(group3.groupName);
        System.out.println(group3.creator);

        Group group4 = new Group();
        group4.groupId = 4;
        group4.groupName = "Nhom 4";
        group4.creator = account4;
        group4.createDate = new Date();

        System.out.println(group4.groupId);
        System.out.println(group4.groupName);
        System.out.println(group4.creator);
        //   Tao GroupAccount
        System.out.println("Them gia tri cho GroupAccount");
        GroupAccount groupAccount1 = new GroupAccount();
        groupAccount1.group = group1;
        groupAccount1.account = account1;
        groupAccount1.joinDate = new Date();

        System.out.println(groupAccount1.account);
        System.out.println(groupAccount1.group);

        GroupAccount groupAccount2 = new GroupAccount();
        groupAccount2.group = group2;
        groupAccount2.account = account2;
        groupAccount2.joinDate = new Date();

        System.out.println(groupAccount2.account);
        System.out.println(groupAccount2.group);

        GroupAccount groupAccount3 = new GroupAccount();
        groupAccount3.group = group3;
        groupAccount3.account = account3;
        groupAccount3.joinDate = new Date();

        System.out.println(groupAccount3.account);
        System.out.println(groupAccount3.group);

        GroupAccount groupAccount4 = new GroupAccount();
        groupAccount4.group = group4;
        groupAccount4.account = account4;
        groupAccount4.joinDate = new Date();

        System.out.println(groupAccount4.account);
        System.out.println(groupAccount4.group);

        //   Tao TypeQuestion
        System.out.println("them gia tri cho TypeQuestion");
        TypeQuestion typeQuestion1 = new TypeQuestion();
        typeQuestion1.typeId = 1;
        typeQuestion1.typeName = "Essay";

        TypeQuestion typeQuestion2 = new TypeQuestion();
        typeQuestion2.typeId = 2;
        typeQuestion2.typeName = "Multiple-Choice";

        //  Tao  CategoryQuestion
        System.out.println("them gia tri cho CategoryQuestion");
        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.categoryName = "Java";
        categoryQuestion1.questionId = 1;

        System.out.println(categoryQuestion1.categoryName);

        CategoryQuestion categoryQuestion2 = new CategoryQuestion();
        categoryQuestion2.categoryName = "SQL";
        categoryQuestion2.questionId = 2;

        System.out.println(categoryQuestion2.categoryName);

        CategoryQuestion categoryQuestion3 = new CategoryQuestion();
        categoryQuestion3.categoryName = "Java";
        categoryQuestion3.questionId = 3;

        System.out.println(categoryQuestion3.categoryName);

        CategoryQuestion categoryQuestion4 = new CategoryQuestion();
        categoryQuestion4.categoryName = "Ruby";
        categoryQuestion4.questionId = 4;

        System.out.println(categoryQuestion4.categoryName);
        // Tao question
        System.out.println("them cho question");
        Question question1 = new Question();
        question1.questionId = 1;
        question1.content = "the nao la java";
        question1.category = categoryQuestion1;
        question1.type = typeQuestion1;
        question1.creator = account1;
        question1.createDate = new Date();

        System.out.println(question1.questionId);
        System.out.println(question1.category);
        System.out.println(question1.creator);
        System.out.println(question1.content);

        Question question2 = new Question();
        question2.questionId = 2;
        question2.content = "the nao la sql";
        question2.category = categoryQuestion2;
        question2.type = typeQuestion2;
        question2.creator = account2;
        question2.createDate = new Date();

        System.out.println(question2.questionId);
        System.out.println(question2.category);
        System.out.println(question2.creator);
        System.out.println(question2.content);

        Question question3 = new Question();
        question3.questionId = 3;
        question3.content = "the nao la java";
        question3.category = categoryQuestion3;
        question3.type = typeQuestion2;
        question3.creator = account3;
        question3.createDate = new Date();

        System.out.println(question3.questionId);
        System.out.println(question3.category);
        System.out.println(question3.creator);
        System.out.println(question3.content);

        // tao answer
        System.out.println("them cho answer");
        Answer answer1 = new Answer();
        answer1.answerId = 1;
        answer1.question = question1;

        Answer answer2 = new Answer();
        answer2.answerId = 2;
        answer2.question = question2;

        Answer answer3 = new Answer();
        answer3.answerId = 3;
        answer3.question = question3;

        // tao Exam
        System.out.println("them cho Exam");
        Exam exam1 = new Exam();
        exam1.category = categoryQuestion1;
        exam1.examId = 1;
        exam1.code = "123";
        exam1.creator = account1;
        exam1.createDate = new Date();

        Exam exam2 = new Exam();
        exam2.category = categoryQuestion2;
        exam2.examId = 2;
        exam2.code = "124";
        exam2.creator = account2;
        exam2.createDate = new Date();

        Exam exam3 = new Exam();
        exam3.category = categoryQuestion3;
        exam3.examId = 3;
        exam3.code = "125";
        exam3.creator = account3;
        exam3.createDate = new Date();

        // Tao cho ExamQuestion
        System.out.println("them cho ExamQuestion");
        ExamQuestion examQuestion1 = new ExamQuestion();
        examQuestion1.exam = exam1;
        examQuestion1.questionId = question1;


        ExamQuestion examQuestion2 = new ExamQuestion();
        examQuestion2.exam = exam2;
        examQuestion2.questionId = question2;

        ExamQuestion examQuestion3 = new ExamQuestion();
        examQuestion3.exam = exam3;
        examQuestion3.questionId = question3;

        // khoi tao cho account
        Account[] arraccount = {account1, account2, account3};
        group1.arraccount = arraccount;

        //
        account2.groups = arrgroup1;
        // --------------BAI TAP SU DUNG IF--------------------
        //ques1
        System.out.println("---------ques1-------");
        if (account2.department == null) {
            System.out.println("nhan vien nay chua co phong ban");
        } else {
            System.out.println("phong ban cua nhan vien nay la" + account2.department.departmentName);
        }

        // ques 2
        System.out.println("-------------ques2-----------");
        if (account2.groups == null) {
            System.out.println("nhan vien nay chua co group");
        } else if (account2.groups.length == 1 || account2.groups.length == 2) {
            System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
        } else if (account2.groups.length == 3) {
            System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
        } else if (account2.groups.length == 4) {
            System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các groups");
        } else {
            System.out.println("khong co van de gi");
        }

        // ques 3 su dung toan tu ternary de lam ques1
        System.out.println("-----------------ques3------------");
        System.out.println(account2.department.departmentName == null ? "Nhân viên này chưa có phòng ban" : "Phòng ban của nhân viên này là" + account2.department.departmentName);

        // ques 4 su dung toan tu ternary
        System.out.println("----------ques4------------");

        System.out.println(account1.position.positionName == "Dev" ? "Đây là Developer" : "Người này không phải là Developer");


        // BAI TAP SU DUNG SWITCH CASE

        // ques 5 lay ra so luong account trong nhom 1 va in ra
        System.out.println("------------------ques5---------");
        int X = group1.arraccount.length;
        switch (X) {
            case 1:
                System.out.println("Nhom co mot thanh vien");
                break;
            case 2:
                System.out.println("nhom co 2 thanh vien");
                break;
            case 3:
                System.out.println("nhom co 3 thanh vien");
                break;
            case 4:
                System.out.println("nhom co nhieu thanh vien");
                break;
        }

        // ques 6 Sử dụng switch case để làm lại Question 2
        System.out.println("----------ques6------------");
        int Y = account2.groups.length;
        if (account2.groups == null) {
            System.out.println("nhan vien nay chua co group");
        } else {
            switch (Y) {
                case 1:
                    System.out.println("nhom co mot thanh vien");
                case 2:
                    System.out.println("nhom co 2 thanh vien");
                case 3:
                    System.out.println("nhom co 3 thanh vien");
                case 4:
                    System.out.println("nhom co nhieu thanh vien");
            }
        }


        // ques 7 Sử dụng switch case để làm lại Question 4
        System.out.println("----------ques7----------");
        String namepositioninaccount1 = account1.position.positionName;
        switch (namepositioninaccount1) {
            case "Dev":
                System.out.println("Đây là Developer");
                break;
            default:
                System.out.println("Nguoi nay khong phai Dev");
        }

        // ---------------BAI TAP FOR EACH---------------

        // ques 8 In ra thông tin các account bao gồm: Email, FullName và tên phòng ban của
        //họ
        System.out.println("------------ques8-------------------");
        Account[] arraccount2 = {account1, account2, account3};
        for (Account account : arraccount2){
            System.out.println(account.fullName);
            System.out.println(account.department.departmentName);
            System.out.println(account.accountId);
        }

        // ques 9 In ra thông tin các phòng ban bao gồm: id và name
        System.out.println("-----------ques9-------------");
        Department[] arrdepartment = {department1, department2, department3, department4};
        for (Department department : arrdepartment){
            System.out.println(department.departmentID);
            System.out.println(department.departmentName);
        }

        // --------------------BAI TAP DUNG FOR----------------
        // 10
        System.out.println("----------------ques10-----------");
        Account[] arraccount3 = {account1, account2, account3};
        for (int i = 0; i < arraccount3.length; i++){
            System.out.println("Thong tin cua account thu" + (i + 1) + "la:");
            System.out.println("Email:" + arraccount3[i].email);
            System.out.println("Fullname:" + arraccount3[i].fullName);
            System.out.println("Phong ban:" + arraccount3[i].department.departmentName);
        }

        // ques 11
        System.out.println("------------------ques11-----------");
        Department[] arrdepartment1 = {department1, department2, department3, department4};
        for (int i = 0; i < arrdepartment1.length; i++){
            System.out.println("Thong tin account thu" + (i + 1) + "la:");
            System.out.println("ID:" + arrdepartment1[i].departmentID);
            System.out.println("Name:" + arrdepartment1[i].departmentName);
        }


        // ques 12 Chỉ in ra thông tin 2 department đầu tiên theo định dạng như Question 10

        System.out.println("-----------ques12----------");
        Department[] arrdepartment2 = {department1, department2, department3, department4};
        for(int i = 0; i <2; i++){
            System.out.println("Thong tin cua department thu" + (i + 1) + "la:");
            System.out.println("Ten phong ban la:" + arrdepartment2[i].departmentName);
            System.out.println("ID cua phong ban nay la:" + arrdepartment2[i].departmentID);
        }

        // ques 13 In ra thông tin tất cả các account ngoại trừ account thứ 2
        System.out.println("------------ques13-------------");
        Account[] arraccount4 = {account1, account2, account3};
        for (int i = 0; i != 1; i++){
            System.out.println("Thong tin cua account thu" + (i + 1) + "la:");
            System.out.println("Email la:" + arraccount4[i].email);
            System.out.println("Fullname la:" + arraccount4[i].fullName);
            System.out.println("phong ban la:" + arraccount4[i].department.departmentName);
        }

        // ques 14 In ra thông tin tất cả các account có id < 4
        System.out.println("--------------ques14-----------");
        Account[] arraccount5 = {account1, account2, account3, account4};
        for(int i = 0; i < arraccount5.length; i++){
            if ( arraccount5[i].accountId < 4){
                System.out.println("thong tin cua acc thu" + (i + 1) + "la:");
                System.out.println("Email:" + arraccount5[i].email);
                System.out.println("Fullname:" + arraccount5[i].fullName);
                System.out.println("ten phong ban la:" + arraccount5[i].department.departmentName);
            }
        }


        // ques 15 In ra các số chẵn nhỏ hơn hoặc bằng 20
        System.out.println("---------ques15---------");
        for (int i = 0; i <= 20; i+= 2){
            System.out.println(i);
        }



















        

    }

}

















